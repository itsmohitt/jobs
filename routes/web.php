<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'jobs@index');
Route::get('profile','HomeController@myProfile');
Route::get('profile/edit','HomeController@editProfile');
Route::post('profile','HomeController@updateProfile');
Route::get('profile/{id}','HomeController@profile');
Auth::routes();

Route::get('job/create','HomeController@createJob');
Route::post('job','HomeController@store');
Route::get('job/{id}','jobs@showDetails');
Route::get('job/{id}/apply','HomeController@applyJob');

Route::get('company/','Controller@companyHome');
Route::get('company/{id}','Controller@companyView');
Route::group(['prefix'=>'company'],function(){
    Route::get('edit','HomeController@companyEdit');
    Route::post('edit','HomeController@companyUpdate');

});

