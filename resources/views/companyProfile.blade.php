<?php
/**
 * Created by PhpStorm.
 * User: bunny
 * Date: 07/05/17
 * Time: 2:54 PM
 */ ?>
@extends('home')
@section('bodyContent')

    <!--header section -->
    <div class="container-fluid page-title">
        <div class="row green-banner">
            <div class="container main-container">
                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                    <h3 class="white-heading">Company Profile</h3>
                </div>
                <div class="col-lg-9 col-md-8 col-sm-6 colxs-12 capital">
                    <h5>Update your company profile and we'll help you find the right candidate</h5>
                </div>
            </div>
        </div>
    </div>
    <!--header section -->


    <!-- full width section forms -->
    <div class="container-fluid  contact_us">
        <form  name="contact_us" id="form-style-2">


            <!-- Company Details-->
            <div class="row company-details">
                <div class="container main-container-home">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="form-group">
                            <label class="heading">Company Details</label>
                        </div>

                        <div class="form-group">
                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                <label class="">Company name</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
                                <input type="text" name="company_name" />
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                <label class="">Company Profile</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
                                <!--   <input type="text" name="company_profile" /> -->
                                <textarea class="" name="company_profile"></textarea>

                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                <label class="default">Website <br /></label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
                                <input type="text" name="website" />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                <label class="default">Recruiter name <br /></label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
                                <input type="text" name="recruiter_name" />
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="form-group ">
                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 zero-padding-left">
                                <label class="default">Recruiter Designation<br /></label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
                                <input type="text" name="recruiter_desg" />
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="form-group file-type ">
                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                <label class="default">Recruiter avatar (optional)<br /><span>(optional)</span></label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
                                <input type="file" name="post-image" class="inputfile" />
                                <div class="upload">
                                    <div class="filename"><i class="fa fa-file-image-o" aria-hidden="true"></i>Browse image </div>
                                    <i>Size should be 250 px 250px</i>
                                </div>
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                <label class="">LinkedIn</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
                                <input type="text" name="linked_in" />
                            </div>
                        </div>



                        <div class="clearfix"></div>

                        <div class="form-group">
                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                <label class="">Facebook</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
                                <input type="text" name="facebook" />
                            </div>
                        </div>

                        <div class="clearfix"></div>

                        <div class="form-group">
                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                <label class="default">Twitter<br /><span>(optional)</span></label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
                                <input type="text" name="twitter" />
                            </div>
                        </div>

                        <div class="clearfix"></div>
                        <div class="form-group file-type image_uplaod ">
                            <label class=""><img id="blah" src="#" alt="your image" /><a href="#" id="removeimg">[remove ]</a></label>
                        </div>
                        <div class="clearfix"></div>
                        <div class="form-group file-type ">
                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                <label class="default">Logo<br /><span>(optional)</span></label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
                                <input type="file" name="post-image" class="inputfile" onchange="readURL(this);"/>
                                <div class="upload">
                                    <div class="filename"><i class="fa fa-file-image-o" aria-hidden="true"></i>Browse image </div>
                                    <i>Size should be 20 MB</i>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>







        </form>
    </div>
    <!-- full width section forms -->

    <!-- Blue Area -->
    <div class="container-fluid green-banner job-page">
        <div class="row">
            <div class="container main-container">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center white-text">
                    <p>Please review your information once more  and press the below button to update your company profile</p>
                    <a href="#" class="btn btn-getstarted bg-red center-small">Submit</a>
                </div>
            </div>
        </div>
    </div>

    <!-- Blue Area -->
@stop