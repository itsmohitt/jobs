@extends('home')
@section('bodyContent')


    <div class="container main-container-home">

        <div class="jobs_results">
            <!--Filters Category -->
            <div class="tab_filters">
                <div class="col-lg-4">
                    <h5>Recent Jobs</h5>
                    <h4>Popular <span>Category </span></h4>
                </div>

                <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 filters pull-right filter-category">
                    <ul class="nav nav-pills">
                        <li class="web-designer"><a href="#">Web Designer</a></li>
                        <li class="fianance"><a href="#">Finance</a></li>
                        <li class="education"><a href="#">Education</a></li>
                        <li class="food-service"><a href="#">Food Services</a></li>
                        <li class="health-services"><a href="#">Health Care</a></li>
                        <li class="automative"><a href="#">Automative</a></li>
                        <li class="all active"><a href="#">All</a></li>
                    </ul>
                </div>
            </div>
            <!-- Filters Category -->
            <div class="jobs-result">
                <!--Search Result 01-->
                <div class="col-lg-12">
                    @foreach($jobs as $job)
                    <a href="{!! URL::to('job/'.$job->JobId) !!}"> <div class="filter-result 01">
                            <div class="col-lg-6 col-md-6 col-sm-7 col-xs-12 pull-left">
                                <div class="company-left-info pull-left">
                                    <img src="assets/images/company-logo.png" alt=""/>
                                </div>
                                <div class="desig">
                                    <h3>{!! $job->jobTitle !!}</h3>
                                    <h4>{!! $job->JobTags !!}</h4>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-5 col-xs-12 pull-right">
                                <div class="pull-right location">
                                    <p>{!! $job->Location !!}</p>
                                </div>
                                <div class="data-job">
                                    <h3>{!! $job->MaxRate !!}</h3>
                                    <span class="label job-type job-contract ">{!! $job->JobType !!}</span>
                                </div>

                            </div>
                        </div> </a>
                    @endforeach
                   {!! $jobs->links() !!}

                <!--Search Result 01-->
                        </div>
            </div>
        </div>

    </div>


    <div class="container-fluid green-banner">
        <div class="row">
            <div class="container main-container v-middle">
                <div class="col-lg-10 col-md-8 col-sm-8 col-xs-12 ">
                    <h3 class="white-heading">Start Building  <span>Your Own Job Board Now</span></h3>
                </div>
                <div class="col-lg-2 col-md-4 col-sm-4 col-xs-12 no-padding-left">
                    <a href="#" class="btn btn-getstarted bg-red">get started now</a>
                </div>
            </div>
        </div>
    </div>
    <!-- Green Banner-->
    <!-- Testimionals Slider-->
    <div class="container-fluid testimionals" style="background:url(assets/images/testbg.png);">
        <div class="row">
            <div class="container main-container">
                <div class="col-lg-12">
                    <div id="testio" class="owl-carousel owl-template">
                        <!--Slides-->
                        <div class="item">
                            <img src="assets/images/tes-profile.png" alt="Photo" />
                            <div class="info">
                                <h5>Anna Smith</h5>
                                <span>Web Designer</span>
                                <p>Nam eu eleifend nulla. Duis consectetur sit amet risus sit amet venenatis. Pellentesque pulvinar ante a tincidunt placerat.
                                    Donec dapibus efficitur arcu, a rhoncus lectus egestas elem</p>
                            </div>
                        </div>

                        <div class="item">
                            <img src="assets/images/tes-profile.png" alt="Photo" />
                            <div class="info">
                                <h5>Anna Smith</h5>
                                <span>Web Designer</span>
                                <p>Nam eu eleifend nulla. Duis consectetur sit amet risus sit amet venenatis. Pellentesque pulvinar ante a tincidunt placerat.
                                    Donec dapibus efficitur arcu, a rhoncus lectus egestas elem</p>
                            </div>
                        </div>

                        <div class="item">
                            <img src="assets/images/tes-profile.png" alt="Photo" />
                            <div class="info">
                                <h5>Anna Smith</h5>
                                <span>Web Designer</span>
                                <p>Nam eu eleifend nulla. Duis consectetur sit amet risus sit amet venenatis. Pellentesque pulvinar ante a tincidunt placerat.
                                    Donec dapibus efficitur arcu, a rhoncus lectus egestas elem</p>
                            </div>
                        </div>

                        <div class="item">
                            <img src="assets/images/tes-profile.png" alt="Photo" />
                            <div class="info">
                                <h5>Anna Smith</h5>
                                <span>Web Designer</span>
                                <p>Nam eu eleifend nulla. Duis consectetur sit amet risus sit amet venenatis. Pellentesque pulvinar ante a tincidunt placerat.
                                    Donec dapibus efficitur arcu, a rhoncus lectus egestas elem</p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Testimionals Slider-->
    <!-- Clients Slider-->
    <div class="container-fluid clients">
        <div class="row">
            <div class="container main-container">
                <div class="col-lg-12">
                    <ul>
                        <li><img src="assets/images/client1.png" alt="Photo" /> </li>
                        <li><img src="assets/images/client2.png" alt="Photo" /> </li>
                        <li><img src="assets/images/client3.png" alt="Photo" /> </li>
                        <li><img src="assets/images/client4.png" alt="Photo" /> </li>
                        <li><img src="assets/images/client1.png" alt="Photo" /> </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- Client Slider-->
@stop