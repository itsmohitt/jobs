<!DOCTYPE html>
<html lang="en">
<!-- Mirrored from deximlabs.com/dexjobs/light/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 23 Apr 2017 11:36:09 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>DEXJOBS- Home</title>
    <link rel="icon" href="{!! URL::to('assets/images/favicon.png') !!}">
    <!-- Bootstrap -->
    <link href="{!! URL::to('assets/css/bootstrap.min.css') !!}" rel="stylesheet">
    <!--Custom template CSS-->
    <link href="{!! URL::to('assets/css/style.css') !!}" rel="stylesheet">
    <!--Custom template CSS Responsive-->
    <link href="{!! URL::to('assets/webcss/site-responsive.css') !!}" rel="stylesheet">
    <!--Animated CSS -->
    <link href="{!! URL::to('assets/webcss/animate.css') !!}" rel="stylesheet">
    <!--Owsome Fonts -->
    <link rel="stylesheet" href="{!! URL::to('assets/font-awesome/css/font-awesome.min.css') !!}">
    <!-- Important Owl stylesheet -->
    <link rel="stylesheet" href="{!! URL::to('assets/owlslider/owl-carousel/owl.carousel.css') !!}">

    <!-- Default template -->
    <link rel="stylesheet" href="{!! URL::to('assets/owlslider/owl-carousel/owl.template.css') !!}">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div id="loadessr"><div id="loader"></div></div>
<!-- Header Image Or May be Slider-->
@include('layouts.header');
<!-- Header Section -->
@yield('bodyContent');

@include('layouts.footer');

<!-- Scripts
================================================== -->
<!--  jQuery 1.7+  -->
<script type="text/javascript" src="{!! URL::to('assets/js/jquery-1.9.1.min.js') !!}"></script>
<!--Select 2-->
<script type="text/javascript" src="{!! URL::to('assets/js/select2.min.js') !!}"></script>
<!-- Html Editor -->
<script src="{!! URL::to('assets/tinymce/tinymce.min.js') !!}"></script>
<!--  parallax.js-1.4.2  -->
<script type="text/javascript" src="{!! URL::to('assets/parallax.js-1.4.2/parallax.js') !!}"></script>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="{!! URL::to('assets/js/bootstrap.min.js') !!}"></script>
<!-- Include js plugin -->
<script type="text/javascript" src="{!! URL::to('assets/owlslider/owl-carousel/owl.carousel.js') !!}"></script>
<script type="text/javascript" src="{!! URL::to('assets/js/waypoints.min.js') !!}"></script>
<script type="text/javascript" src="{!! URL::to('assets/counter/jquery.counterup.min.js') !!}"></script>
<!--Site JS-->
<script src="{!! URL::to('assets/js/webjs.js') !!}"></script>

<script>
    $(window).load(function() {

        $("#loadessr").fadeOut();

    })
</script>


<!-- Scripts
================================================== -->
</body>

<!-- Mirrored from deximlabs.com/dexjobs/light/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 23 Apr 2017 11:36:49 GMT -->
</html>