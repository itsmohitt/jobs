@extends('home')
@section('bodyContent')
    <!-- Page Title-->
    <div class="container-fluid page-title dashboard">
        <div class="row blue-banner">
            <div class="container main-container">
                <div class="col-lg-8 col-md-8 col-sm-8">
                    <h3 class="white-heading">Software Developer</h3> <!-- (from applicant.Curr_Job_Profile) -->
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <div class="favourite short">Save resume<i class="fa fa-star-o"></i></div>
                </div>
            </div>
        </div>

        <div class="row dashboard">
            <div class="container main-container gery-bg">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  no-padding user-data">
                    <div class="seprator ">
                        <div class="no-padding user-image"><img src="assets/images/job-admin.png" alt=""/></div>
                        <div class="user-tag">{!! (isset($applicant->Current_Job_Profile)?$applicant->Current_Job_profile:'') !!}<span>{!! $user->name !!}</span></div>

                    </div>
                    <div class="seprator">
                        <div class="jos-status"><span class="label job-type job-partytime">Part time </span></div> <!-- (from applicant.Interested_Job_Type)-->

                        <!--   <div class="user-tag"><label>Sallary<span>$35000 - $38000</span></label></div> -->
                    </div>

                    <div class="seprator">
                        <div class="user-tag"><label>Location<span>{!! (isset($applicant->Current_Location)?$applicant->Current_Location:'') !!}</span></label></div> <!-- (from applicant.Curr_Location) -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Page Titl!e-->

    <!-- Job Data -->
    <div class="container-fluid white-bg">
        <div class="row">
            <div class="container main-container-job">
                <div class="col-lg-9 col-md-8 col-sm-12 col-xs-12">
                    <div class="">
                        <h3 class="small-heading">My skills:</h3>
                        <ul class="skills"> <!-- (from applicant.Skills) - comma_seprated -->
                            <li>Photoshop</li>
                            <li>Javascript</li>
                            <li>Wordpress</li>
                            <li>HTML</li>
                            <li>Designe</li>
                        </ul>
                    </div>
                    <div class="content">
                        <h3 class="small-heading">About Me</h3> <!-- (from applicant.About)-->
                        <p>{!! (isset($applicant->About)?$applicant->About:'') !!}</p>

                        <!--    <h3 class="small-heading">Personal Characteristics:</h3>
                            <ul>
                                <li>Excellent customer service skills, communication skills, and a happy, smiling attitude are essential.</li>
                                <li>Available to work required shifts including weekends, evenings and holidays.</li>
                                <li>I have great time management skills.</li>
                                <li>I take constructive criticism well and I am comfortable voicing opinions.</li>
                          </ul> -->

                        <h3 class="small-heading">Education</h3>
                        <ul class="education">
                            <li>
                                <div class="data-row">
                                    <div class="date">{!! (isset($applicant->Grad_Year_From)?$applicant->Grad_Year_From:'') !!} - {!! (isset($applicant->Grad_Year_To)?$applicant->Grad_Year_To:'') !!} </div>
                                    <h3>{!! (isset($applicant->Grad_Course_Name)?$applicant->Grad_Course_Name:'') !!}</h3>
                                    <p>{!! (isset($applicant->Grad_Institute)?$applicant->Grad_Institute:'') !!}</p>
                                    <h4>Agg. - {!! (isset($applicant->Grad_Marks)?$applicant->Grad_Marks:'') !!} %</h4>
                                </div>
                            </li>
                            <li>
                                <div class="data-row">
                                    <div class="date">{!! (isset($applicant->Intermediate_Year)?$applicant->Intermediate_Year:'') !!}</div>
                                    <h3>Intermediate (Board - CBSE )</h3>
                                    <p>{!! (isset($applicant->Intermediate_Institute)?$applicant->Intermediate_Institute:'') !!}</p>
                                    <h4>Agg. - {!! (isset($applicant->Intermediate_Marks)?$applicant->Intermediate_Marks:'') !!} %</h4>
                                </div>
                            </li>
                            <li>
                                <div class="data-row">
                                    <div class="date">{!! (isset($applicant->High_School_Year)?$applicant->High_School_Year:'') !!}</div>
                                    <h3>HighSchool (Board - CBSE)</h3>
                                    <p>{!! (isset($applicant->High_School_Institute)?$applicant->High_School_Institute:'') !!}</p>
                                    <h4>Agg. - {!! (isset($applicant->High_School_Marks)?$applicant->High_School_Marks:'') !!} %</h4>

                                </div>
                            </li>
                        </ul>


                        <h3 class="small-heading">Projects</h3>
                        <ul class="education">

                            <li>
                                <div class="data-row">
                                    <div class="date">2012-2015 </div>
                                    <h3>{!! (isset($applicant->Project1)?$applicant->Project1:'') !!}</h3>
                                    <p>{!! (isset($applicant->Des1)?$applicant->Des1:'') !!}</p>
                                </div>
                            </li>

                            <!-- INCLUDE THIS IF PROJECT 2 ENTERIES ARE NOT NULL --> 
                            
                                 @if( isset($applicant->Project2) && isset($applicant->Des2) && $applicant->Project2 != NULL && $applicant->Des2 != NULL)

                                 <li>
                                     <div class="data-row">
                                         <div class="date">2012-2015 </div>
                                         <h3>{!! (isset($applicant->Project2)?$applicant->Project2:'') !!}</h3>
                                         <p>{!! (isset($applicant->Des2)?$applicant->Des2:'') !!}</p>
                                     </div>
                                 </li> 

                                 @endif

                        </ul>


                    </div>
                </div>

                <div class="col-lg-3 col-md-4 col-sm-12 col-xs-12 sidebar">
                    <div class="widget w1">
                        <ul>
                            <li>
                                @if(Auth::user()->id == $user->id)
                                    <span class="label job-type apply-job"><a href="{!! URL::to('/profile/edit') !!}">Edit Profile</a></span>
                                @elseif(Auth::user()->role == 'RECRUITER')
                                    <span class="label job-type apply-job">Hire me</span>
                                @endif

                                <span class="label job-type apply-link">my profile on<i class="fa fa-linkedin"></i></span>
                            </li>

                        </ul>
                        <ul class="social">
                            <li><a href="#"><i class="fa fa-link"></i>www</a></li>
                            <li><a href="#"><i class="fa fa-facebook"></i>facebook</a></li>
                        </ul>
                    </div>




                </div>
            </div>
        </div>

    </div>
    <!-- Job Data-->

    <!--Blue Secions --> <div class="container-fluid green-banner" >
        <div class="row">
            <div class="container main-container v-middle" id="style-2">
                <div class="col-lg-10 col-md-8 col-sm-8 col-xs-12  ">
                    <h3 class="white-heading">Got a question?<span class="call-us">send us an email or <strong>call us at 1 (800) 555-5555</strong></span></h3>
                </div>
                <div class="col-lg-2 col-md-4 col-sm-12 col-xs-12">
                    <a href="#" class="btn btn-getstarted bg-red">get started now</a>
                </div>
            </div>
        </div>
    </div>
    <!--blue Section -->

@stop