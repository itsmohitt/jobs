@extends('home')

@section('bodyContent')
    <div class="container-fluid login_register header_area deximJobs_tabs">
        <div class="row">
            <div class="container main-container-home">
                <div class="col-lg-offset-1 col-lg-11 col-md-12 col-sm-12 col-xs-12">
                    <ul class="nav nav-pills ">
                        <li class="active"><a data-toggle="tab" href="#register-account">Register</a></li>
                        <li><a data-toggle="tab" href="#login">Login</a></li>

                    </ul>

                    <div class="tab-content">
                        <div id="register-account" class="tab-pane fade in active white-text">

                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 zero-padding-left">
                                <p>Login to your Recruiter account.</p>
                                <form  role="form" method="POST" action="{{ route('register') }}">
                                    {{ csrf_field() }}

                                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                        <label for="name" class="col-md-4 control-label">Name</label>

                                        <div class="col-md-8">
                                            <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                            @if ($errors->has('name'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                        <label for="email" class="col-md-4 ">E-Mail Address</label>

                                        <div class="col-md-8">
                                            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                            @if ($errors->has('email'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                        <label for="password" class="col-md-4 ">Password</label>

                                        <div class="col-md-8">
                                            <input id="password" type="password" class="form-control" name="password" required>

                                            @if ($errors->has('password'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="password-confirm" >Confirm Password</label>

                                        <div class="col-md-8">
                                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Role</label>
                                        <div class="col-md-8">
                                            <select class="form-control" name="role">
                                                <option value="recruiter">Company</option>
                                                <option value="applicant">Job Applicant</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-6 col-md-offset-4">
                                            <button type="submit" class="btn btn-primary">
                                                Register
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="col-lg-4 col-md-5 col-sm-6 col-xs-12  pull-right sidebar">
                                <div class="widget">
                                    <h3>Why to have  an account in dexjobs? </h3>
                                    <ul>
                                        <li><p><i class="fa fa-clock-o"></i>Fast applying for offers
                                                the necessary documents are always at hand</p></li>
                                        <li><p><i class="fa fa-child"></i>Effectively target employers
                                                You can share your profile and CV recruiters</p></li>

                                        <li><p><i class="fa fa-check-circle-o"></i>Matching jobs
                                                We will suggest you offer to fit your needs on e-mail</p></li>
                                    </ul>

                                </div>
                            </div>
                        </div>
                        <div id="login" class="tab-pane fade in  white-text">

                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 zero-padding-left">
                                <p>Login to your Recruiter account.</p>
                                <form name="contact_us" method="post" class="contact_us">
                                    {!! csrf_field() !!}
                                    <div class="form-group">
                                        <label>Email</label>
                                        <input type="text" name="email">
                                    </div>

                                    <div class="form-group">
                                        <label>Password</label>
                                        <input type="password" name="password" />
                                    </div>

                                    <div class="form-group submit">
                                        <label>Submit</label>
                                        <div class="cbox">
                                            <input type="checkbox" name="checkbox"/>
                                            <span>Remember me</span>
                                        </div>
                                    </div>
                                    <div class="form-group submit">
                                        <label>Submit</label>
                                        <input type="submit" name="submit" value="Sign in" class="signin" id="signin">
                                        <a href="lost-password.html" class="lost_password">Lost Password?</a>
                                    </div>


                                </form>
                            </div>
                            <div class="col-lg-4 col-md-5 col-sm-6 col-xs-12  pull-right sidebar">
                                <div class="widget">
                                    <h3>Don't have an account?</h3>
                                    <ul>
                                        <li>
                                            <p> If you'd like to find out more about how Jobsite can help you with your recruitment needs, please complete this enquiry form.</p></li>
                                        <li><p>A member of our Sales team will contact you shortly.</p></li>
                                        <li>
                                            <a href="#" class="label job-type register">Register</a>

                                        </li>
                                    </ul>

                                </div>
                            </div>
                        </div>

                    </div>


                </div>

            </div>
        </div>
    </div>

@endsection
