<?php
/**
 * Created by PhpStorm.
 * User: Billy
 * Date: 4/23/2017
 * Time: 5:54 PM
 */
        ?>
<div id="header" class="container-fluid home">
    <div class="row">
        @if(isset($index) && $index==1)
        <div class="header_banner">
            <div class="slides">
                <div class="slider_items parallax-window"  data-parallax="scroll" data-image-src="assets/images/headerimage1.jpg"></div>
            </div>
        </div>
        @endif
        <!-- Header Image Or May be Slider-->
        <div class="top_header">
            <nav class="navbar navbar-fixed-top">

                <div class="container">
                    <div class="logo">
                        <a href="{!! URL::to('/') !!}"><img src="{!! URL::to('assets/images/logo2.png') !!}" alt="Photo" /> </a>
                    </div>
                    <div class="logins">


                        @if(Auth::guest())
                            <a href="{!! URL::to('/login') !!}" class="post_job"><span class="label job-type partytime">Login</span></a>
                        @else
                            @if(Auth::user()->role == "RECRUITER" )
                                <a href="post-a-job.html" class="post_job"><span class="label job-type partytime">POST A JOB, IT’S FREE!</span></a>
                            @endif
                            <a href="{{ url('/logout') }}"
                               onclick="event.preventDefault();
                                                                             document.getElementById('logout-form').submit();" class="post_job">
                                <span class="label job-type partytime">  Logout </span>
                            </a>

                            <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        @endif


                    </div>
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav">

                            <li class="dropdown">
                                <a href="{!! URL::to('/') !!}" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Home
                                    <span class="sr-only">(current)</span></a>     <li class="mobile-menu add-job"><a href="post-a-job.html">POST A JOB, IT’S FREE!</a></li>
                            <ul class="dropdown-menu">

                                <li><a href="home2.html">Home#2</a></li>

                            </ul>
                            </li>

                            <li class="dropdown">
                                <a href="{!! URL::to('/#jobs_filters_id') !!}"  role="button" aria-haspopup="true">Browse Jobs</a>
                              
                            </li>

                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Pages</a>
                                <ul class="dropdown-menu">
                                    <li><a href="job-style-one.html">Job Style One</a></li>
                                    <li><a href="job-style-two.html">Job Style Two</a></li>
                                    <li><a href="job-preview.html">Job Preview</a></li>
                                    <li><a href="resume.html">Resume Page</a></li>
                                    <li><a href="companies.html">Companies</a></li>


                                    <li><a href="pricing_table.html">Pricing Table</a></li>
                                </ul>
                            </li>
                            <li><a href="blog.html">Blog</a></li>
                            <li><a href="how_its_work.html">How It Works</a></li>
                            <li><a href="contact-us.html">Contact</a></li>
                            <li class="mobile-menu"><a href="post-a-job.html">POST A JOB, IT’S FREE!</a></li>
                            @if(Auth::guest())
                            <li class="mobile-menu "><a href="login_register.html" >Register User</a>
                            </li>
                            @else
                           <li class="mobile-menu "> <a href="{{ url('/logout') }}"
                                                                       onclick="event.preventDefault();
                                                                             document.getElementById('logout-form').submit();">
                                                                        Logout
                                                                    </a></li>
                            @endif
                        </ul>

                    </div><!-- navbar-collapse -->


                </div>
                <!-- container-fluid -->
            </nav>
            @if(isset($index) && $index==1)
            <div class="container slogan">
                <div class="col-lg-12">
                    <h1 class="animated fadeInDown">Looking For a Job?</h1>
                    <h3 class="text-center"><span>Join us </span>& Explore thousands of jobs</h3>
                    <a href="#">We have <span>59</span> jobs offers for you!</a>
                </div>

            </div>
            @endif
        </div>
            @if(isset($index) && $index==1)
                <div class="jobs_filters" id="jobs_filters_id">
                    <div class="container">
                        <form class="" action="http://deximlabs.com/dexjobs/light/index.html">
                            <!--col-lg-3 filter_width -->
                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 filter_width bgicon">
                                <div class="form-group">
                                    <div class="dropdown">
                                        <button class="filters_feilds btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                            Category
                                            <span class="glyphicon glyphicon-menu-down"></span>
                                        </button>

                                        <div class="dropdown-menu "  aria-labelledby="dropdownMenu1">
                                            <ul class="tiny_scrolling" id="style-3">
                                                <li><a href="#">Web Developer</a></li>
                                                <li><a href="#">Graphic designer</a></li>
                                                <li><a href="#">Developer</a></li>
                                                <li><a href="#">UX Designer</a></li>
                                                <li><a href="#">Web Developer</a></li>
                                                <li><a href="#">Graphic designer</a></li>
                                                <li><a href="#">Developer</a></li>
                                                <li><a href="#">UX Designer</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <span>e.g. Finance</span>
                            </div>
                            <!--col-lg-3 filter_width -->

                            <!-- col-lg-5 filter_width -->
                            <div class="col-lg-5 col-md-4 col-sm-6 col-xs-12 filter_width bgicon">
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Keyword, job title or skill">
                                    <span class="glyphicon glyphicon-briefcase" aria-hidden="true"></span>
                                </div>
                                <span>e.g. Designer</span>
                            </div>
                            <!-- col-lg-5 filter_width -->

                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 filter_width bgicon location">
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Location">
                                    <span class="glyphicon fa fa-location-arrow" aria-hidden="true"></span>
                                </div>
                                <span>e.g. New York</span>
                            </div>
                            <div class="col-lg-1 col-md-2 col-sm-6 col-xs-12 filter_width bgicon submit">
                                <div class="form-group">
                                    <input type="submit" class="customsubmit" name="submit" value="Search"/>
                                    <span class="glyphicon fa fa-search" aria-hidden="true"></span>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            @endif
        </div>

</div>
