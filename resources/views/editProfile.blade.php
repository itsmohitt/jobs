<?php
/**
 * Created by PhpStorm.
 * User: bunny
 * Date: 07/05/17
 * Time: 1:12 PM
 */?>
@extends('home')
@section('bodyContent')
    <!--header section -->
    <div class="container-fluid page-title">
        <div class="row green-banner">
            <div class="container main-container">
                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                    <h3 class="white-heading">Profile</h3>
                </div>
                <div class="col-lg-9 col-md-8 col-sm-6 colxs-12 capital">
                    <h5>Complete your profile so that we can help you find your dream job</h5>
                </div>
            </div>
        </div>
    </div>
    <!--header section -->


    <!-- full width section forms -->
    <div class="container-fluid  contact_us">
        <form  name="contact_us" id="form-style-2">


            <div class="row user-information">
                <div class="container main-container-home">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                        <div class="form-group">
                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                <label>Name</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
                                <input type="text" name="name" placeholder="from database and readonly" value=""/>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                <label>Email</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
                                <input type="text" name="email" placeholder="from database and readonly"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                <label>Interested Job category</label>
                            </div>

                            <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
                                <select class="form-control" name="jobCat" id="tagPicker" multiple="multiple">
                                    @foreach($jobCats as $jobCat)
                                        <option value="{!! $jobCat->id !!}">{!! $jobCat->att_description !!}</option>
                                    @endforeach

                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                <label>Interested Job type</label>
                            </div>

                            <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
                                <select class="form-control" class="jobType" id="tagPicker" multiple="multiple">
                                    @foreach($jobTypes as $jobType)
                                        <option value="{!! $jobType->id !!}">{!! $jobType->att_description !!}</option>
                                    @endforeach
                                </select>
                            </div>

                            <!--   <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
                                   <input type="text" name="job-type" placeholder="Select Full Time / Part Time"/>
                               </div> -->

                        </div>

                        <div class="form-group">
                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                <label>Current Location</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
                                <input type="text" name="curr_location" value="{!! (isset($applicant->Current_Location)?$applicant->Current_Location:'') !!}"placeholder="Enter Current Working Location. e.g- Delhi"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                <label>Current Profile</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
                                <input type="text" name="curr_profile" value="{!! (isset($applicant->Current_Job_Profile)?$applicant->Current_Job_profile:'') !!}" placeholder="Enter Current Job / Working Profile. e.g - Full Stack Developer"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                <label>About Yourself</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
                                <textarea name="about" class="">{!! (isset($applicant->About)?$applicant->About:'') !!}</textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                <label>Linked In Profile</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
                                <input type="text" name="linkedIn" value="{!! (isset($applicant->Linkedin_Id)?$applicant->Linkedin_Id:'') !!}" placeholder="Enter Your Linked In Profile"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                <label>Facebook Profile</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
                                <input type="text" name="fb" value="{!! (isset($applicant->Fb_Id)?$applicant->Fb_Id:'') !!}" placeholder="Enter Your Facebook Profile"/>
                            </div>
                        </div>



                    </div>
                </div>
            </div>
            <!-- User Data Row-->





            <!-- HighSchool Details-->
            <div class="row company-details">
                <div class="container main-container-home">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                        <div class="form-group submit">
                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">

                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
                                <span>Write about your Educational Qualifications</span>
                            </div>
                        </div><br><br><br>

                        <div class="form-group">
                            <label class="heading">HighSchool / Equivalent Details</label>
                        </div>

                        <div class="form-group">
                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                <label class="">Name Of Institute</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
                                <input type="text" name="highSchoolName"  value="{!! (isset($applicant->High_School_Institute)?$applicant->High_School_Institute:'') !!}" />
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                <label class="default">Board <br /></label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
                                <select class="form-control" name="highSchoolBoard" >
                                    @foreach($boards as $board)
                                        <option value="{!! $board->id !!}">{!! $board->att_description !!}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">

                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                <label class="default">Year <br /></label>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <input type="text" name="highSchoolYear"  value="{!! (isset($applicant->High_School_Year)?$applicant->High_School_Year:'') !!}"/>
                            </div>

                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                <label class="default">Agg. % Marks (Out of 100)<br /></label>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <input type="text" name="highSchoolMarks"  value="{!! (isset($applicant->High_School_Marks)?$applicant->High_School_Marks:'') !!}"/>
                            </div>


                        </div>


                    </div>
                </div>
            </div>


            <!-- Intermediate Details-->
            <div class="row company-details">
                <div class="container main-container-home">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                        <div class="form-group">
                            <label class="heading">Intermediate / Equivalent Details</label>
                        </div>

                        <div class="form-group">
                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                <label class="">Name Of Institute</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
                                <input type="text" name="intermediateName"  value="{!! (isset($applicant->Intermediate_Institute)?$applicant->Intermediate_Institute:'') !!}"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                <label class="default">Board <br /></label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
                                <select class="form-control" name="intermediateBoard" >
                                    @foreach($boards as $board)
                                        <option value="{!! $board->id !!}">{!! $board->att_description !!}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">

                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                <label class="default">Year <br /></label>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <input type="text" name="intermediateYear"  value="{!! (isset($applicant->Intermediate_Year)?$applicant->Intermediate_Year:'') !!}"/>
                            </div>

                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                <label class="default">Agg. % Marks (Out of 100)<br /></label>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <input type="text" name="IntermediateMarks" value="{!! (isset($applicant->Intermediate_Marks)?$applicant->Intermediate_Marks:'') !!}"/>
                            </div>


                        </div>

                    </div>
                </div>
            </div>




            <!-- Graduation Details-->
            <div class="row company-details">
                <div class="container main-container-home">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                        <div class="form-group">
                            <label class="heading">Graduation Details</label>
                        </div>

                        <div class="form-group">
                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                <label class="">Name Of Institute</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
                                <input type="text" name="graduationName" value="{!! (isset($applicant->Grad_Institute)?$applicant->Grad_Institute:'') !!}"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                <label class="">Name Of Course</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
                                <input type="text" name="graduationCourse" value="{!! (isset($applicant->Grad_Course_Name)?$applicant->Grad_Course_Name:'') !!}"/>
                            </div>
                        </div>
                        <div class="form-group">

                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                <label class="default">Starting Year <br /></label>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <input type="text" name="graduationStartYear" value="{!! (isset($applicant->Grad_Year_From)?$applicant->Grad_Year_From:'') !!}"/>
                            </div>

                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                <label class="default">Completion Year<br /></label>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <input type="text" name="graduationEndYear" value="{!! (isset($applicant->Grad_Year_To)?$applicant->Grad_Year_To:'') !!}"/>
                            </div>


                        </div>

                        <div class="form-group">
                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                <label class="default">Agg. % Marks (Out of 100)<br /></label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
                                <input type="text" name="graduationMarks" value="{!! (isset($applicant->Grad_Marks)?$applicant->Grad_Marks:'') !!}"/>
                            </div>
                        </div>


                    </div>
                </div>
            </div>


            <div class="row user-info">
                <div class="container main-container-home">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                        <div class="form-group submit">
                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                <label></label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
                                <span>Write about your Skills and Projects</span>
                            </div>
                        </div>


                        <div class="form-group">

                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                <label class="default">Skills <br /></label>
                            </div>

                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                <input type="text" name="skill[]" />
                            </div>

                            <!-- <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                                 <label><br /></label>
                             </div> -->

                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                <input type="text" name="skill[]" />
                            </div>

                            <!--  <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                                  <label><br /></label>
                              </div> -->

                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                <input type="text" name="skill[]" />
                            </div>

                            <!--  <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                                <label><br /></label>
                            </div> -->

                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                <input type="text" name="skill[]" />
                            </div>

                            <!--  <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                                <label><br /></label>
                            </div> -->

                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                <input type="text" name="skill[]" />
                            </div>


                        </div>


                        <div class="form-group">
                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                <label class="">Name Of Project 1</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
                                <input type="text" name="projectName1" value="{!! (isset($applicant->Project1)?$applicant->Project1:'') !!}"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                <label>Project Description 1</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
                                <textarea class="" name="projectDesc1">{!! (isset($applicant->Des1)?$applicant->Des1:'') !!}</textarea>
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                <label class="">Name Of Project 2</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
                                <input type="text" name="projectName2" value="{!! (isset($applicant->Project2)?$applicant->Project2:'') !!}"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                <label>Project Description 2</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
                                <textarea class="" name="projectDesc2">{!! (isset($applicant->Des2)?$applicant->Des2:'') !!}</textarea>
                            </div>
                        </div>



                        <div class="form-group file-type ">
                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                <label class="default">Upload Resume <br /></label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
                                <input type="file" name="resume" class="inputfile"/>

                                <div class="upload">
                                    <div class="filename"><i class="fa fa-file-image-o" aria-hidden="true"></i>Browse Resume </div>
                                    <i>Size should be 850 px 350 px</i>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </form>
    </div>
    <!-- full width section forms -->

    <!-- Blue Area -->
    <div class="container-fluid green-banner job-page">
        <div class="row">
            <div class="container main-container">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center white-text">
                    <p>Please review your information once more  and press the below button to put your job online</p>
                    <a href="#" class="btn btn-getstarted bg-red center-small">Submit</a>
                </div>
            </div>
        </div>
    </div>
@stop
