<?php

namespace App\Http\Controllers;

use App\admin_dropdowns;
use App\applicant;
use App\recruiter;
use Illuminate\Http\Request;
use Auth;
use App\User;
use App\job;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }
    public function createJob(){
        return view('jobCreate');
    }
    public function store(Request $request)
    {
        $job = new job;
        $job->jobTitle = $request['name'];
        $job->JobType = $request['job-type'];
        $job->Location = $request['location'];
        $job->JobTags = $request['job-tag'];
        $job->Description = $request['description'];
        $job->JobURL = $request['application-email'];
        $job->MaxRate = $request['rate'];
        $job->save();
        //var_dump($job);
          return redirect('/');

    }
    public function myProfile(){
            $user = User::where('id','=',Auth::user()->id)->first();
            $applicant = applicant::where('UserId','=',Auth::user()->id)->first();
            return view('profile')->with(['user'=>$user,'applicant'=>$applicant]);

    }
    public function editProfile(){
        $user = User::where('id','=',Auth::user()->id)->first();
        $jobCats = admin_dropdowns::where('dropdown_name','=','JOB_CATEGORY')->get();
        $borads =  admin_dropdowns::where('dropdown_name','=','SCHOOL_BOARDS')->get();
        $jobTypes = admin_dropdowns::where('dropdown_name','=','JOB_TYPE')->get();
        $applicant = applicant::where('UserId','=',Auth::user()->id)->first();
        return view('editProfile')->with(['user'=>$user,'jobCats'=>$jobCats,'boards'=>$borads,'jobTypes'=>$jobTypes,'applicant'=>$applicant]);
    }
    public function updateProfile(Request $req){
        $user = User::where('id','=',Auth::user()->id)->first();
        $applicant = applicant::where('UserId','=',Auth::user()->id)->first();
        if($applicant== NULL){
            $applicant = new applicant;
        }
        $applicant->About = $req['about'];
        $applicant->LinkedIn_Id = $req['linkedIn'];
        $applicant->Fb_Id = $req['fb'];
        $applicant->High_School_Year = $req['highSchoolYear'];
        $applicant->High_School_Institute = $req['highSchoolName'];
        $applicant->High_School_Board = $req['highSchoolBoard'];
        $applicant->High_School_Marks = $req['highSchoolMarks'];
        $applicant->Intermediate_Year = $req['intermediateYear'];
        $applicant->Intermediate_Institute = $req['intermediateName'];
        $applicant->Intermediate_Board = $req['intermediateBoard'];
        $applicant->Intermediate_Marks = $req['intermediateMarks'];
        $applicant->Grad_Year_From = $req['graduationStartYear'];
        $applicant->Grad_Year_To = $req['graduationEndYear'];
        $applicant->Grad_Course_Name = $req['graduationCourse'];
        $applicant->Grad_Institute = $req['graduationName'];
        $applicant->Grad_Marks = $req['graduationMarks'];
        $skills = $req['skill'];
        $applicant->Skills = implode(',',$skills);
        $applicant->Project1 = $req['projectName1'];
        $applicant->Des1 = $req['projectDesc1'];
        //$applicant->Project1_Duration = $req[''];
        $applicant->Project2 = $req['projectName2'];
        $applicant->Des2 = $req['projectDesc2'];
        //$applicant->Project2_Duration = $req[''];
        $applicant->Interested_Job_Category = $req['jobCat'];
        $applicant->Interested_Job_Type = $req['jobType'];
        $applicant->Curr_Location = $req['curr_location'];
        $applicant->Curr_Job_Profile = $req['curr_profile'];
        $applicant->save();
        return redirect(URL::to('/profile'));


    }
    public function profile($id){
        $user = User::where('id','=',$id)->first();
        $applicant = applicant::where('UserId','=',$id)->first();
        return view('profile')->with(['user'=>$user,'applicant'=>$applicant]);
    }

    public function companyEdit(){
        $user = User::where('id','=',Auth::user()->id)->first();
        $recruiter = recruiter::where('UserId','=',Auth::user()->id)->first();
        return view('companyProfile')->with(['user'=>$user,'recruiter'=>$recruiter]);
    }
    public function companyUpdate(Request $req){
        $user = User::where('id','=',Auth::user()->id)->first();
        $recruiter = recruiter::where('UserId','=',Auth::user()->id)->first();
        if($recruiter == NULL){
            $recruiter = new recruiter;
        }
        $recruiter->UserId = Auth::user()->id;
        $recruiter->Company_Name = $req['company_name'];
        $recruiter->Company_Profile = $req['company_profile'];
        $recruiter->Website = $req['website'];
        $recruiter->Recruiter_Name = $req['recruiter_name'];
        $recruiter->Recruiter_Position = $req['recruiter_desg'];
        $recruiter->FB = $req['facebook'];
        $recruiter->Twiiter = $req['twiiter'];
        $recruiter->LinkedIn = $req['linked_in'];
        $recruiter->save();
        return redirect(URL::to('/company/'.Auth::user()->id));

    }
}
