<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class admin_dropdowns extends Model
{
    protected $table = "admin_dropdowns";
    protected $fillable = [
        'id',
        'dropdown_name',
        'att_name',
        'att_description'
    ];
}
