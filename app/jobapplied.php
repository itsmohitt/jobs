<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class jobapplied extends Model
{
    protected $table = 'jobapplied';
    protected $fillable = [
        'JobApplyId', 'UserId','JobId', 'created_at', 'updated_at'
    ];
}
