<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class recruiter extends Model
{
    protected $table = 'recruiter';
    protected $fillable = [
        'RecId',
        'UserId',
        'ComapnyName',
        'Website',
        'Recruiter',
        'RecruiterPos',
        'Fb',
        'Twitter',
        'LinkedIn',
        'created_at',
        'updated_at'
    ];
}
