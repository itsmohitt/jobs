<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class applicant extends Model
{
    protected $table = 'applicant';
    protected $fillable = [
        'AppId',
        'UserId',
        'About',
        'Linkedin_Id',
        'Fb_Id',
        'High_School_Year',
        'High_School_Institute',
        'High_School_Board',
        'High_School_Marks',
        'Intermediate_Year',
        'Intermediate_Institute',
        'Intermediate_Board',
        'Intermediate_Marks',
        'Grad_Year_From',
        'Grad_Year_To',
        'Grad_Course_Name',
        'Grad_Institute',
        'Grad_Marks',
        'Skills',
        'Project1',
        'Des1',
        'Project1_Duration',
        'Project2',
        'Des2',
        'Project2_Duration',
        'Interested_Job_Category',
        'Interested_Job_Type',
        'Curr_Job_Profile',
        'Curr_Location',
        'Resume',
        'created_at',
        'updated_at'
    ];
}
