<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class job extends Model
{
    protected $table = 'job';
    protected $fillable = [
        'JobId',
        'jobTitle',
        'JobType',
        'Location',
        'JobTags',
        'Description',
        'JobURL',
        'MaxRate',
        'created_at',
        'updated_at'
    ];
}
