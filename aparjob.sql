-- MySQL dump 10.13  Distrib 5.7.14, for Win64 (x86_64)
--
-- Host: localhost    Database: jobportal
-- ------------------------------------------------------
-- Server version	5.7.14

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `applicant`
--

DROP TABLE IF EXISTS `applicant`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `applicant` (
  `AppId` int(10) NOT NULL AUTO_INCREMENT,
  `UserId` int(10) DEFAULT NULL,
  `About` varchar(5000) DEFAULT NULL,
  `Resume` varchar(50000) DEFAULT NULL,
  `created_at` timestamp NOT NULL,
  `updated_at` timestamp NOT NULL,
  PRIMARY KEY (`AppId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `applicant`
--

LOCK TABLES `applicant` WRITE;
/*!40000 ALTER TABLE `applicant` DISABLE KEYS */;
/*!40000 ALTER TABLE `applicant` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `job`
--

DROP TABLE IF EXISTS `job`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `job` (
  `JobId` int(10) NOT NULL AUTO_INCREMENT,
  `jobTitle` varchar(100) DEFAULT NULL,
  `JobType` varchar(25) DEFAULT NULL,
  `Location` varchar(50) DEFAULT NULL,
  `JobTags` varchar(1000) DEFAULT NULL,
  `Description` varchar(5000) DEFAULT NULL,
  `JobURL` varchar(150) DEFAULT NULL,
  `MaxRate` varchar(20) DEFAULT NULL,
  `created_at` timestamp NOT NULL,
  `updated_at` timestamp NOT NULL,
  PRIMARY KEY (`JobId`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `job`
--

LOCK TABLES `job` WRITE;
/*!40000 ALTER TABLE `job` DISABLE KEYS */;
INSERT INTO `job` VALUES (1,'Senior Web Developer','Full Time','Gurgaon','Angular, HTML5 , ReactJS ,PHP , Python','<h3>Google INC</h3>\n                            <p>You will help Google build next-generation web applications like Gmail, Google Docs, Google Analytics, and the Google eBookstore and eBook readers. As a Front End Engineer at Google, you will specialize in building responsive and elegant web UIs with AJAX and similar technologies. You may design or work on frameworks for building scalable frontend applications. We are looking for engineers who are passionate about and have experience building leading-edge user experience, including dynamic consumer experiences.</p>\n                       \n                        <h2>Primary Responsibilities:</h2>\n                        <ul>\n                            <li>Take action to avoid potential hazards and obstructions, such as utility lines.</li>\n                            <li>Start engines, move throttles, switches, and levers, and depress pedals</li>\n                            <li>Align machines with reference stakes and guidelines following hand signals of other workers.</li>\n                            <li>Load and move dirt, rocks, equipment, and materials, using trucks, crawler tractors, shovels, graders, or related equipment.</li>\n                            <li>Coordinate machine actions with other activities, positioning or moving loads in response to hand.</li>\n                        </ul>\n                         <h2>Good to have:</h2>\n                         <ul class=\"simple\">\n                         	<li>Wordpress</li>\n                            <li>HTML5</li>\n                            <li>CSS</li>\n                            <li>Javascript</li>\n                            <li>SEO</li>\n                         </ul>',NULL,'50000','2017-04-23 16:23:22','2017-04-23 16:23:22'),(2,'Job Title','Free Time','London',NULL,NULL,NULL,NULL,'2017-04-23 11:37:24','2017-04-23 11:37:24'),(3,'Job Title','Free Time','London',NULL,NULL,NULL,NULL,'2017-04-23 11:37:45','2017-04-23 11:37:45'),(4,'New JOb',NULL,NULL,NULL,NULL,NULL,NULL,'2017-04-23 11:39:00','2017-04-23 11:39:00'),(5,'New JOb',NULL,NULL,NULL,NULL,NULL,NULL,'2017-04-23 11:39:42','2017-04-23 11:39:42'),(6,'Max Jobs',NULL,NULL,NULL,NULL,NULL,NULL,'2017-04-23 11:40:37','2017-04-23 11:40:37'),(7,'Max Jobs',NULL,NULL,NULL,NULL,NULL,NULL,'2017-04-23 11:40:51','2017-04-23 11:40:51');
/*!40000 ALTER TABLE `job` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jobapplied`
--

DROP TABLE IF EXISTS `jobapplied`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jobapplied` (
  `JobApplyId` int(10) NOT NULL,
  `UserId` int(10) DEFAULT NULL,
  `JobId` int(10) DEFAULT NULL,
  `Message` varchar(500) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jobapplied`
--

LOCK TABLES `jobapplied` WRITE;
/*!40000 ALTER TABLE `jobapplied` DISABLE KEYS */;
/*!40000 ALTER TABLE `jobapplied` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2016_10_31_090714_create_department_table',1),(3,'2016_10_31_090735_create_job_posts_table',1),(4,'2016_10_31_205830_create_application_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `recruiter`
--

DROP TABLE IF EXISTS `recruiter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `recruiter` (
  `RecId` int(10) NOT NULL AUTO_INCREMENT,
  `UserId` varchar(10) DEFAULT NULL,
  `ComapnyName` varchar(100) DEFAULT NULL,
  `Website` varchar(100) DEFAULT NULL,
  `Recruiter` varchar(100) DEFAULT NULL,
  `RecruiterPos` varchar(100) DEFAULT NULL,
  `Fb` varchar(500) DEFAULT NULL,
  `Twitter` varchar(500) DEFAULT NULL,
  `LinkedIn` varchar(500) DEFAULT NULL,
  `created_at` timestamp NOT NULL,
  `updated_at` timestamp NOT NULL,
  PRIMARY KEY (`RecId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `recruiter`
--

LOCK TABLES `recruiter` WRITE;
/*!40000 ALTER TABLE `recruiter` DISABLE KEYS */;
/*!40000 ALTER TABLE `recruiter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `role` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'abc',
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Demo Admin','admin@gmail.com','ADMIN','$2y$10$Ayn.NN0sv.3cpgtp4x7Uo.fwnN249cqiEOeG7mV2DgCLHkMV1RwQ.',NULL,NULL,NULL),(2,'MOHIT GUPTA','itsmohitt@gmail.com','abc','$2y$10$rTVy.4EYTyoLUC7ZXfEI3OUKCJcsSihFnu.9YQp4i3MjSz6jCHLyW',NULL,'2017-04-23 07:48:47','2017-04-23 07:48:47');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-05-06 11:18:39
